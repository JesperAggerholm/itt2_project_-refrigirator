**Make sure that you set up the ATmega328p first!**

## Install the OS

* Install Raspbian Stretch Lite on the Raspberry ( Guide can be found [here](https://medium.com/@danidudas/install-raspbian-jessie-lite-and-setup-wi-fi-without-access-to-command-line-or-using-the-network-97f065af722e) ) 
* Access the Raspberry

## Transfer the python code

* Transfer [simple_connection.py](Minimal System/Pi code/simple_connection.py) to the Raspberry Pi 1.

## Connect Raspberry Pi 1 to the ATmega328P board

* A picture of how to do it is found [here](/Minimal System/images/Hcsmr6x_1_.png)

* In the bash terminal enter following command -

        python3 simple_connection.py
    
The program should be up and running!



