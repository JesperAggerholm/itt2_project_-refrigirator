## Write code to ATmega328P

* Download the content of this folder: [DataLogger](https://gitlab.com/laur176n/itt2_project_-refrigirator/tree/master/Minimal%20System/ATmega328%20code/DataLogger)
* Download Atmel studio from the following link - [Atmel Studio 7 download](https://www.microchip.com/mplab/avr-support/atmel-studio-7)
* Install Atmel studio.
* Connect the ATmega328P to a PC with a micro-USB to USB cable.
* Open Atmel studio and click on File -> Open -> Project/Solution, in the file browser navigate to project_main.atsln and select it.

![IMG1](https://i.imgur.com/iCaRESM.png)

![IMG2](https://i.imgur.com/tdI5tLO.png)
* Click on Debug -> Start Without Debugging

![IMG3](https://i.imgur.com/O6buahG.png)
* Wait for the Atmel studio to finish and then the program is ready for flashing!